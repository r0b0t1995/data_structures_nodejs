#!/usr/bin/node

function bubble(list_array){
    aux = 0;
    for(let i = 0; i < (list_array.length - 1); i++){
        for(let j = 0; j < (list_array.length - 1); j++){
            if(list_array[j] > list_array[j + 1]){
                aux = list_array[j];
                list_array[j] = list_array[j + 1];
                list_array[j + 1] = aux;
            }
        }
    }
}

let array = [5,2,7,3,8,1,4,9,6];

bubble(array);

console.log(array)

// npm install 
// node Bubble_sort.js