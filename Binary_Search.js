#!/usr/bin/node

let array = [100,200,300,400,500,600,700,800,900]

let target = 400;

let left = index =  result = 0;
let right = (array.length - 1);


while(left <= right){
    index = Math.round(((left + right) / 2));
    if(array[index] == target){
        result = index;
        break;
    }
    if(array[index] > target){
        right = index - 1;
    }else{
        left = index + 1;
    }
}
console.log(target + " is in index: " + result);

// npm install
// node Binary_Search.js

// in linux 
// chmod +x Binary_Search.js
// ./Binary_Search.js :)