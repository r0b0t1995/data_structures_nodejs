#!/usr/bin/node 
let selection = (list_array) => {
    let aux = 0;
    for(let i = 0; i < (list_array.length - 1); i++){
        let min = i;
        for(let j = i; j < list_array.length; j++){
            if(list_array[min] > list_array[j]){
                min = j;
            }
        }
        aux = list_array[min];
        list_array[min] = list_array[i];
        list_array[i] = aux;
    }
}

let array = [14,11,5,10,2,12,7,3,15,8,13,1,4,9,6];

selection(array);

console.log(array)

// npm install 
// node Selection_sort.js