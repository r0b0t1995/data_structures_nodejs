#!/usr/bin/node
let quick = (list, first, last) => {
    let i = first;
    let j = last;
    let pivote = ((list[first] + list[last]) / 2);
    while(i < j){

        while(list[i] < pivote){
            i++;
        }

        while(list[j] > pivote){
            j--;
        }

        if(i <= j){
            let aux = list[i];
            list[i] = list[j];
            list[j] = aux;
            i++;
            j--;
        }
    }

    if(i < last){
        quick(list, i, last);
    }

    if(j > first){
        quick(list, first, j);
    }
}

let array = [35,26,14,21,32,11,37,31,19,30,22,16,34,25,5,10,2,23,17,29,36,12,7,18,24,27,28,3,15,33,8,20,13,1,4,9,6];

quick(array, 0, (array.length - 1));

console.log(array);

// npm install
// node Quick_Sort.js


// in linux chmod +x Quick_Sort.js
// ./Quick_Sort.js