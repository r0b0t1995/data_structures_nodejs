#!/usr/bin/node

function merge (list) {

    if(list.length > 1){
        let middle = Math.round(list.length / 2);
        let first_list = list.slice(0, middle);
        let last_list = list.slice(middle, list.length);

        merge(first_list);
        merge(last_list);

        i=j=k=0;

        while(i < first_list.length && j < last_list.length){
            if(first_list[i] < last_list[j]){
                list[k] = first_list[i];
                i++;
            }else{
                list[k] = last_list[j];
                j++;
            }
            k++;
        } 
        
        while(i < first_list.length){
            list[k] = first_list[i];
            i++;
            k++;
        }

        while(j < last_list.length){
            list[k] = last_list[j];
            j++;
            k++;
        }
    }
}

let array = [14,21,11,19,22,16,5,10,2,23,17,12,7,18,3,15,8,20,13,1,4,9,6];

merge(array)

console.log(array);



//node install
// node Merge_sort.js
// by Robert Chaves P:)